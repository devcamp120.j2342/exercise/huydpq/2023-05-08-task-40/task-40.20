"use strict";

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
var gCourseId
let gSTT = 1;
//biến mảng hằng số chứa ds tên các thuộc tính
const gCOURSE_COLS = ["id", "coverImage", "courseCode", "courseName", "level", "duration", "price", "discountPrice", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gSTT_COL = 0;
const gCOURSE_IMAGE = 1;
const gCOURSE_CODE_COL = 2;
const gCOURSE_NAME_COL = 3;
const gLEVEL_COL = 4;
const gDURATION_COL = 5;
const gPRICE_COL = 6;
const gDISCOUNT_PRICE_COL = 7;
const gTEACHER_NAME_COL = 8;
const gTEACHER_PHOTO_COL = 9;
const gIS_POPULAR_COL = 10;
const gIS_TRENDING_COL = 11;
const gACTION_COL = 12;
// hàm render ô checked
function getChecked(paramData) {
    var vCheck = `<input type= "checkbox" checked= "checked" disabled: true >`
    if (!paramData) {
        vCheck = `<input type= "checkbox" disabled: true >`
    }
    return vCheck
}
//khai báo DataTable & mapping columns
var gCourseTable = $("#course-table").DataTable({
    columns: [
        { data: gCOURSE_COLS[gSTT_COL] },
        { data: gCOURSE_COLS[gCOURSE_IMAGE] },
        { data: gCOURSE_COLS[gCOURSE_CODE_COL] },
        { data: gCOURSE_COLS[gCOURSE_NAME_COL] },
        { data: gCOURSE_COLS[gLEVEL_COL] },
        { data: gCOURSE_COLS[gDURATION_COL] },
        { data: gCOURSE_COLS[gPRICE_COL] },
        { data: gCOURSE_COLS[gDISCOUNT_PRICE_COL] },
        { data: gCOURSE_COLS[gTEACHER_NAME_COL] },
        { data: gCOURSE_COLS[gTEACHER_PHOTO_COL] },
        { data: gCOURSE_COLS[gIS_POPULAR_COL] },
        { data: gCOURSE_COLS[gIS_TRENDING_COL] },
        { data: gCOURSE_COLS[gACTION_COL] }

    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            render: function () {
                return gSTT++
            }
        },
        {
            targets: gCOURSE_IMAGE,
            render: function (data) {
                return `<img src = "` + data + `" "height = 100%" " width = 100%" />`
            }
        },
        {
            targets: gPRICE_COL,
            render: function (data) {
                return data + " $"
            }
        },
        {
            targets: gDISCOUNT_PRICE_COL,
            render: function (data) {
                return data + " $"
            }
        },
        {
            targets: gTEACHER_PHOTO_COL,
            class: "text-center",
            render: function (data) {
                return `<img src = "` + data + `" "height = 75%" " width = 75%" class = "rounded-circle"/>`
            }
        },
        {
            targets: gIS_POPULAR_COL,
            render: function (data) {
                return getChecked(data)
            }
        },
        {
            targets: gIS_TRENDING_COL,
            render: function (data) {
                return getChecked(data)
            }
        },
        {
            targets: gACTION_COL,
            class: "text-center",
            defaultContent: `
            <i class="far fa-edit fa-lg btn-edit"  title = "Edit" style="color: green; cursor:pointer"></i> &nbsp; 
            <i class="fas fa-trash-alt fa-lg btn-delete"  title = "Delete" style="color: red; cursor:pointer"></i>
            `
        }
    ]
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();

    // nút thêm mới 
    $("#btn-add-new-course").on("click", () => {
        onBtnAddCourseClick()
    })

    //gán sự kiện cho nút Add Course(trên modal)
    $("#btn-create-course").on("click", function () {
        onBtnCreateCourseClick();
    });

    // gán sự kiện update-sửa 1 course
    $("#course-table").on("click", ".btn-edit", function () {
        onBtnEditCourseClick(this);
    });
    //gán sự kiện cho nút Update course(trên modal)
    $("#btn-update-course").on("click", function () {
        onBtnUpdateCourseClick();
    });

    // gán sự kiện delete-xóa 1 course
    $("#course-table").on("click", ".btn-delete", function () {
        onBtnDeleteCourseClick(this);
    });

    //gán sự kiện cho nút Delete course(trên modal)
    $("#btn-confirm-delete-course").on("click", function () {
        onBtnDeleteCourseConfirmClick();
    });

});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    getAllCourses();
}

//hàm xử lý sự kiện nút Thêm mới
function onBtnAddCourseClick() {
    $("#modal-add-course").modal("show")
}

// hàm CREATE USER 
function onBtnCreateCourseClick() {
    var OjbCreate = {
        courseCode: "",
        courseName: "",
        coverImage: "",
        level: "",
        isPopular: false,
        isTrending: false,
        duration: "",
        teacherName: "",
        teacherPhoto: "",
        price: "",
        discountPrice: "",
    }
    // thu thập dữ liệu
   getCreateCourseData(OjbCreate);
    // kiểm tra
    var vIsCheck = validateCourseDate(OjbCreate)
    if (vIsCheck) {
        $.ajax({
            url: gBASE_URL + "/courses",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(OjbCreate),
            success: function (paramCreateOjb) {

                // xử lý front-end
                handleCreateCourseSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });

    }
}
//hàm xử lý sự kiện Update course từ table
function onBtnEditCourseClick(paramEdit) {
    //lưu thông tin CourseId đang dc edit vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramEdit);
    console.log("Id của course tương ứng = " + gCourseId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getCourseDataByCourseId(gCourseId);
}

//hàm xử lý sự kiện nút Update course(trên modal)
function onBtnUpdateCourseClick() {
    var OjbUpdate = {
        courseCode: "",
        courseName: "",
        coverImage: "",
        level: "",
        isPopular: false,
        isTrending: false,
        duration: "",
        teacherName: "",
        teacherPhoto: "",
        price: 0,
        discountPrice: 0,
    }
    // thu thập dữ liệu
    getEditCourseData(OjbUpdate);
    // kiểm tra
    var vIsCheck = validateCourseDate(OjbUpdate)
    if (vIsCheck) {
        $.ajax({
            url: gBASE_URL + "/courses/" + gCourseId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(OjbUpdate),
            success: function (paramEdit) {

                // xử lý front-end
                handleUpdateCourseSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện Delete course từ table
function onBtnDeleteCourseClick(paramDelete) {
    //lưu thông tin userId đang dc xóa vào biến toàn cục
    gCourseId = getCourseIdFromButton(paramDelete);
    console.log("Id của course tương ứng = " + gCourseId);
    $("#modal-delete-course").modal("show");
}
// hàm xử lý sự kiện delete course modal click
function onBtnDeleteCourseConfirmClick() {
    //  Thu thập dữ liệu(k có)
    // Validate delete(k có)
    //  call api to delete course
    $.ajax({
        url: gBASE_URL + "/courses/" + gCourseId,
        type: "DELETE",
        contentType: "application/json;charset=UTF-8",
        success: function (paramRes) {
            // xử lý front-end
            handleDeleteCourseSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm thu thập create data
function getCreateCourseData(paramCreate) {
    paramCreate.courseCode = $("#inp-create-coursecode").val().trim();
    paramCreate.courseName = $("#inp-create-coursename").val().trim();
    paramCreate.coverImage = $("#inp-create-coverimage").val().trim();
    paramCreate.level = $("#select-create-level").val();

    // is Popular
   
    var vPopular = $("#select-popular").val();
    if (vPopular != "0") {
        var vKiemTraBooleanPopular = false
        if (vPopular == "true") {
            vKiemTraBooleanPopular = true
        }
        paramCreate.isPopular = vKiemTraBooleanPopular
    } else {
        paramCreate.isPopular = "0"
    }

    // is Trending
    var vTrending = $("#select-trending").val();
    if (vTrending != "0") {
        var vKiemTraBooleanTrending = false
        if (vTrending == "true") {
            vKiemTraBooleanTrending = true
        }
        paramCreate.isTrending = vKiemTraBooleanTrending
    } else {
        paramCreate.isTrending = "0"
    }
    paramCreate.duration = $("#inp-create-duration").val().trim();
    paramCreate.teacherName = $("#inp-create-teachername").val().trim();
    paramCreate.teacherPhoto = $("#inp-create-teacherphoto").val().trim();
    paramCreate.price = $("#inp-create-price").val().trim();
    paramCreate.discountPrice = $("#inp-create-discountprice").val().trim();

   
}
//hàm thu thập update data
function getEditCourseData(paramEdit) {
    paramEdit.courseCode = $("#inp-edit-coursecode").val().trim();
    paramEdit.courseName = $("#inp-edit-coursename").val().trim();
    paramEdit.coverImage = $("#inp-edit-coverimage").val().trim();
    paramEdit.level = $("#select-edit-level").val();
    paramEdit.duration = $("#inp-edit-duration").val().trim();
    paramEdit.teacherName = $("#inp-edit-teachername").val().trim();
    paramEdit.teacherPhoto = $("#inp-edit-teacherphoto").val().trim();
    // is Popular
    var vPopular = false
    if ($("#select-edit-popular").val() == "true") {
        vPopular = true
    }
    paramEdit.isPopular = vPopular
    // is Trending
    var vTrending = false
    if ($("#select-edit-trending").val() == "true") {
        vTrending = true
    }
    paramEdit.isTrending = vTrending
    
    paramEdit.price = $("#inp-edit-price").val().trim();
    paramEdit.discountPrice = $("#inp-edit-discountprice").val().trim();
}

//hàm show course data lên update modal form
function showCourseDataToUpdateModal(paramCourseDataObj) {
    $("#inp-edit-coursecode").val(paramCourseDataObj.courseCode);
    $("#inp-edit-coursename").val(paramCourseDataObj.courseName);
    $("#inp-edit-coverimage").val(paramCourseDataObj.coverImage);
    $("#select-edit-level").val(paramCourseDataObj.level);
    $("#inp-edit-duration").val(paramCourseDataObj.duration);
    $("#inp-edit-teachername").val(paramCourseDataObj.teacherName);
    $("#inp-edit-teacherphoto").val(paramCourseDataObj.teacherPhoto);
    $("#inp-edit-price").val(paramCourseDataObj.price);
    $("#inp-edit-discountprice").val(paramCourseDataObj.discountPrice);
    // is Popular
    var vPopular = paramCourseDataObj.isPopular
    if (vPopular) {
        $("#select-edit-popular").val("true")
    } else {
        $("#select-edit-popular").val("false")
    }
    // is Trending
    var vTrending = paramCourseDataObj.isTrending
    if (vTrending == true) {
        $("#select-edit-trending").val("true")
    } else {
        $("#select-edit-trending").val("false")
    }
    // hiển thị modal lên
    $("#modal-update-course").modal("show");
}

// hàm Validate Data
function validateCourseDate(paramCourseObj) {
    if (paramCourseObj.courseCode === "") {
        alert("Course Code is not blank!");
        return false;
    }
    if (paramCourseObj.courseCode.length < 10) {
        alert('Course Code must at least 10 characters.');
        return false;
    }
    if (paramCourseObj.courseName === "") {
        alert("Course Name is not blank!");
        return false;
    }
    if (paramCourseObj.courseName.length < 20) {
        alert('Course Name must at least 20 characters.');
        return false;
    }
    if (paramCourseObj.coverImage === "") {
        alert("Cover Image is not blank!");
        return false;
    }
    if (paramCourseObj.level == "0") {
        alert("Chưa chọn level");
        return false;
    }
    if (paramCourseObj.isPopular === "0") {
        alert("Chưa chọn Popular");
        return false;
    }
    if (paramCourseObj.isTrending === "0") {
        alert("Chưa chọn Trending");
        return false;
    }
    if (paramCourseObj.duration === "") {
        alert("Duration is not blank!");
        return false;
    }
    if (paramCourseObj.teacherName === "") {
        alert("Teacher Name is not blank!");
        return false;
    }
    if (paramCourseObj.teacherPhoto === "") {
        alert("Teacher Photo is not blank!");
        return false;
    }
    if (paramCourseObj.price === "") {
        alert("Price is not blank!");
        return false;
    }
    if (!isFinite(paramCourseObj.price)) {
        alert("Price phải là số!");
        return false;
    }
    if (!isFinite(paramCourseObj.discountPrice)) {
        alert("Discount Price phải là số!");
        return false;
    }
    if (parseInt(paramCourseObj.discountPrice) >= parseInt(paramCourseObj.price)) {
        alert("Discount Price must not be higher than price!");
        return false;
    }
    return true;

}
//hàm dựa vào button detail (edit or delete) xác định dc id course
function getCourseIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vCourseRowData = gCourseTable.row(vTableRow).data();
    return vCourseRowData.id;
}

//hàm load course data lên update modal form
function getCourseDataByCourseId(paramCourseId) {
    $.ajax({
        url: gBASE_URL + "/courses/" + paramCourseId,
        type: "GET",
        success: function (paramCourseId) {
            console.log(paramCourseId)
            showCourseDataToUpdateModal(paramCourseId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });


}


// //hàm xử lý front-end khi add course thành công
function handleCreateCourseSuccess() {
    alert("Thêm thành công");
    $("#modal-add-course").modal("hide");
    resetCreateUserForm(); //reset trắng lại form modal add course
    getAllCourses(); //load lại table

}

//hàm xử lý front-end khi update user thành công
function handleUpdateCourseSuccess() {
    alert("Update thành công");
    $("#modal-update-course").modal("hide");
    getAllCourses();

}

//hàm xử lý front-end khi delete course thành công
function handleDeleteCourseSuccess() {
    alert("Delete course successfully!");
    getAllCourses();
    $("#modal-delete-course").modal("hide");
}


function getAllCourses() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function (paramCourses) {
            //ghi response ra console
            console.log(paramCourses)
            loadDataToTable(paramCourses);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });

}

// hàm load all dữ liệu lên table
function loadDataToTable(paramData) {
    gSTT = 1
    gCourseTable.clear()
    gCourseTable.rows.add(paramData)
    gCourseTable.draw()
}
// hàm xóa trắng modal Create 
function resetCreateUserForm() {
    $("#inp-create-coursecode").val("");
    $("#inp-create-coursename").val("");
    $("#inp-create-coverimage").val("");
    $("#select-create-level").val("0");
    $("#select-popular").val("0");
    $("#select-trending").val("0");
    $("#inp-create-duration").val("");
    $("#inp-create-teachername").val("");
    $("#inp-create-teacherphoto").val("");
    $("#inp-create-price").val("");
    $("#inp-create-discountprice").val("");

}